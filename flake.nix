{
  description = "Personal site (again)";

  inputs = {
    devenv.url = "github:cachix/devenv";
    flake-parts.url = "github:hercules-ci/flake-parts";
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.05";
  };

  outputs = inputs@{ flake-parts, ... }:
    flake-parts.lib.mkFlake { inherit inputs; } {
      imports = [
        inputs.devenv.flakeModule
      ];
      systems = [ "x86_64-linux" ];
      perSystem = { pkgs, lib, ... }:
        let
          inherit (lib) importJSON;
          inherit (pkgs) mkYarnPackage runCommand;
          pname = (importJSON ./package.json).name;
        in
        {
          packages = {
            "${pname}" =
              let
                prodNodeModulesPkg = pkgs.mkYarnPackage {
                  src = pkgs.runCommand "${pname}-prod-deps" { } ''
                    mkdir $out
                    cp ${./package.json} $out/package.json
                    cp ${./yarn.lock} $out/yarn.lock
                  '';
                  yarnFlags = [ "--prod" ];
                };
              in
              pkgs.mkYarnPackage {
                name = "${pname}-build";
                src = ./.;
                nativeBuildInputs = [ pkgs.makeWrapper ];

                configurePhase = ''
                  cp -r "$node_modules" node_modules
                  chmod -R u+w node_modules
                '';

                buildPhase = ''
                  yarn build
                '';

                installPhase = ''
                  runHook preInstall

                  mkdir -p $out/share
                  cp -R {dist,package.json} $out/share
                  cp -R ${prodNodeModulesPkg}/libexec/${pname}/node_modules $out/share/node_modules

                  runHook postInstall
                '';

                distPhase = ''
                  runHook preDist

                  mkdir $out/bin

                  cat > $out/bin/${pname} <<EOF
                    #!${pkgs.stdenv.shell}/bin/sh
                    ${pkgs.nodejs}/bin/node $out/share/dist/server.js
                  EOF
                  chmod +x $out/bin/${pname}
                  wrapProgram $out/bin/${pname} \
                    --set NODE_PATH "$out/share/node_modules"

                  runHook postDist
                '';
              };
          };

          devenv.shells.default = {
            enterShell =
              let
                nodeModulesPkg = mkYarnPackage {
                  src = runCommand "${pname}-dev-deps" { } ''
                    mkdir $out
                    cp ${./package.json} $out/package.json
                    cp ${./yarn.lock} $out/yarn.lock
                  '';
                };
              in
              ''
                if [[ -d node_modules || -L node_modules ]]; then
                  echo "./node_modules is present. Replacing."
                  rm -rf node_modules
                fi

                ln -s ${nodeModulesPkg}/libexec/${pname}/node_modules node_modules
              '';
            packages = [ pkgs.yarn ];
            languages.javascript.enable = true;
            languages.typescript.enable = true;
          };
        };
    };
}
