# Personal Site (again)

I have wanted a personal site for a long time. Hope this time I can finally finish it with [solid-start](https://start.solidjs.com);

## Developing

```sh
# dependencies
yarn install

# or (if using nix,direnv)
direnv allow

# start development server
yarn run dev
```

## Build
```sh
# build production server
yarn build

# strip out devDependencies
rm -rf node_modules && yarn install --prod

# or (if using nix)
nix build .#personal-site-reboot
```
