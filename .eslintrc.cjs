module.exports = {
  env: {
    browser: true,
    es2021: true
  },
  extends: [
    'airbnb-base',
    'airbnb-typescript/base',
    'plugin:@typescript-eslint/recommended',
    'plugin:@typescript-eslint/recommended-requiring-type-checking',
    'eslint:recommended',
    'plugin:import/recommended',
    'plugin:import/typescript',
    'plugin:solid/typescript',
  ],
  plugins: [
    '@typescript-eslint',
    'solid',
    'react',
    'import',
  ],
  settings: {
    'import/resolver': {
      typescript: true,
      node: true,
    }
  },
  overrides: [
    {
      env: {
        node: true
      },
      files: [
        '.eslintrc.{js,cjs}'
      ],
    }
  ],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaVersion: 'latest',
    project: ['./tsconfig.json'],
    sourceType: 'module',
  },
  rules: {
    'react/jsx-indent': [2, 2],
    'react/jsx-indent-props': [2, 2],
    'react/jsx-closing-bracket-location': [2, 'tag-aligned'],
    'no-param-reassign': 'off',
    'no-warning-comments': 'warn',
    'comma-dangle': 'off',
    '@typescript-eslint/comma-dangle': ['warn', {
      'generics': 'ignore', // disable generics checking as the format ```<T,>(v: T) => ...``` is required for tsx
      'arrays': 'always-multiline',
      'objects': 'always-multiline',
      'imports': 'always-multiline',
      'exports': 'always-multiline',
      'functions': 'always-multiline',
      'enums': 'always-multiline',
      'tuples': 'always-multiline',
    }],
    'max-len': ['warn', { code: 100 }],
    'import/prefer-default-export': 'off',
    'react/jsx-uses-react': 'error',
    'react/jsx-uses-vars': 'error',
    // I love my arrow function components ;w;
    // https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/function-component-definition.md
    'react/function-component-definition': ['error', {
      namedComponents: 'arrow-function',
      unnamedComponents: 'arrow-function',
    }],
    // let tsserver check props instead of eslint
    'react/prop-types': 'off',
    'react/require-default-props': 'off',
    '@typescript-eslint/naming-convention': 'off',
    '@typescript-eslint/no-unused-vars': ["warn", {
      "argsIgnorePattern": "^_",
      "varsIgnorePattern": "^_",
    }],
    'no-underscore-dangle': 'off',
  }
}
