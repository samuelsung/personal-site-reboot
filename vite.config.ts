import solid from 'solid-start/vite';
import eslint from 'vite-plugin-eslint'; // eslint-disable-line import/no-extraneous-dependencies
import { defineConfig } from 'vite'; // eslint-disable-line import/no-extraneous-dependencies

export default defineConfig({
  plugins: [solid(), eslint()],
  server: {
    watch: {
      ignored: [
        '**/.devenv/**',
        '**/.direnv/**',
        '**/.git/**',
        '**/node_modules/**',
      ],
    },
  },
  cacheDir: '.vite',
});
